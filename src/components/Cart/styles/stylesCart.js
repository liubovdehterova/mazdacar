import {createUseStyles} from "react-jss";

export const useStyleCart = createUseStyles({
    star: {
        width: "25px",
        height: "25px"
    },
    listItem: {
        border: '1px solid #000000'
    },
    avatar: {
        width: '100% !important',
        height: '75px !important'
    }
});