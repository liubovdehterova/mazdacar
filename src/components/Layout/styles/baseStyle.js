import {createUseStyles} from "react-jss";

export const useStyleBase = createUseStyles({
    container: {
        maxWidth: "1450px",
        margin: "0 auto"
    }
})